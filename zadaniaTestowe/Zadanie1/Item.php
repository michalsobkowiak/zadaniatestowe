<?php


class Item
{
    private $itemQuantity;
    private $itemName;
    private $itemPrice;

    public function __construct($itemName,$itemPrice,$itemQuantity)
    {
        $this->itemName = $itemName;
        $this->itemPrice = $itemPrice;
        $this->itemQuantity = $itemQuantity;
    }

    /**
     * @return mixed
     */
    public function getItemName()
    {
        return $this->itemName;
    }

    /**
     * @return mixed
     */
    public function getItemPrice()
    {
        return $this->itemPrice;
    }

    public function getItemQuantity(){
        return $this->itemQuantity;
    }

}