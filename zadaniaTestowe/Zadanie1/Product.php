<?php


class Product
{
    private $productName;
    private $price;
    protected $minOrderQuantity = 1;

    function __construct($productName,$price,$minOrderQuantity)
    {   $this->minOrderQuantity = $minOrderQuantity;
        $this->productName = $productName;
        $this->price = $price;
    }

    /* Metody używane wyłacznie jako parametry konstruktora przy tworzeniu obiektu Item w koszyku
       lub przy usuwaniu danej pozycji z koszyka */
    public function getProductName(){
        return $this->productName;
    }

    public function getPrice(){
        return $this->price;
    }
    public function getMinQuantity(){
        return $this->minOrderQuantity;
    }

}
