<?php


class Cart
{
    private $orderList = array(); //Pusta tablica reprezentująca liste produktów w koszyku

    public function addProduct($productQuantity,Product $productName){

        /* Sprawdzanie czy istnieje w koszyku pozycja 'item' o nazwie produktu 'productName'. Jeśli nie istnieje to dodajemy pozycje 'item' do koszyka
           w przeciwnym wypadku zwiększamy wyłacznie ilość sztuk danej pozycji w koszyku */

            if (!array_search($productName->getProductName(), array_column($this->orderList, 'Product'))) {

                $ourItem = new Item($productName->getProductName(), $productName->getPrice(), $productQuantity); //Tworzenie nowej pozycji 'item' w koszyku

                $this->orderList[] = ['Product' => $ourItem->getItemName(), 'Price' => $ourItem->getItemPrice(), 'Quantity' => $ourItem->getItemQuantity()];
            } else {
                // Zwiększanie ilości w przypadku istnienia danej pozycji
                for ($i = 0; $i < count($this->orderList); $i++) {
                    if ($this->orderList[$i]['Product'] == $productName->getProductName()) {
                        $this->orderList[$i]['Quantity'] += $productQuantity;
                    }
                }
            }

    }
    public function removeProduct(Product $productName){
        for($i = 0; $i<count($this->orderList); $i++) {
            if($this->orderList[$i]['Product'] == $productName->getProductName()){
               array_splice($this->orderList,$i,1);
            }

        }

    }
    public function showOrder(){
        echo '<h1>Your current order:</h1>';
        $keys = array_keys($this->orderList);
            for($i = 0; $i< count($this->orderList); $i++){
                foreach ($this->orderList[$keys[$i]] as $item => $value) {
                    echo $item . ' - ' . $value;
                    echo '<br>';
                }
                echo '<br>';
            }
    }
}